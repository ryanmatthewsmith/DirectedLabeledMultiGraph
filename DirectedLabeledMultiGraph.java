package hw5;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * DirectedLabeledMultiGraph represents a mutable directed multigraph
 * with labeled edges.
 * 
 * @author RyanMatthewSmith
 * @version 0.0
 */
public class DirectedLabeledMultiGraph {
   
   /** Holds all graph vertices, edges */
   private HashMap<String,ArrayList<EdgeOut>> vertices;
   
   /** boolean, when false more efficient checkRep */
   private final static boolean CHECK_FULL = false;
   
   /**
    *  Abstraction Function:
    *     DirectedLabeledMultiGraph, g, represents a set of vertices in the graph
    *     g = {}, an empty set, represents empty graph
    *     g = {v_0,v_1,...,v_n-1}, when graph is non-empty with n vertices
    *     forall v_x in g
    *       v_x has a list of directed out edges, e
    *           e = [] empty list when v_x has no out edges
    *           e = [a_0,a_1,...,a_m-1] where each a is an out edge and m is # of out edges
    *                a_y is a single out edge
    *                a = (ver,lab), a contains ver: a destination vertex and lab: a label for out edge. 
    *     
    *  Representation Invariant:
    *    vertices != null
    *    forall v in g: v != null && v is unique
    *      forall e in v
    *         e!=null
    */
   
   /**
    * Constructs and empty directed multigraph
    * 
    * @effects Constructs new empty MultiGraph
    */
   public DirectedLabeledMultiGraph(){	  
      vertices = new HashMap<String,ArrayList<EdgeOut>>();
      checkRep();
   }
   
   /**
    *  Adds a new vertex to graph
    *  
    *  @param v, a String representing a vertex
    *  @throws IllegalArgumentException when v = null
    *  @modifies (this)
    *  @effects adds vertex into the graph if it doesn't exist already
    *  @return return true if vertex added successfully
    */
   public boolean addVertex(String v){
      checkRep();
      if (v == null){
         throwIAE("addVertex requires non-null, nonempty arg");  
      }
      
      if (vertices.containsKey(v)){
         return false;
      }
      
      vertices.put(v,new ArrayList<EdgeOut>());
      checkRep();
      return true;
   }
   
   /**
    *  Removes a vertex from graph
    * 
    *  @param v, a String representing a vertex
    *  @throws IllegalArgumentException when v = null
    *  @modifies (this)
    *  @effects removes vertex from graph
    *  @return return true if vertex removed successfully
    *  
    */
   public boolean removeVertex(String v){
      checkRep();
      if (v == null){
         throwIAE("addVertex requires non-null, nonempty arg");  
      }
      
      if (!vertices.containsKey(v)){
         return false;
      }
      
      vertices.remove(v);
      checkRep();
      return true;
   }
   
   /**
    *  Returns true if vertex is in graph
    *  
    *  @param v, a String representing a vertex
    *  @throws IllegalArgumentException if v == null
    *  @return returns true if vertex exists in graph
    */
   public boolean containsVertex(String v){
      if (v == null){
         throwIAE("containsVertex requires non-null, nonempty arg");  
      }
      return vertices.containsKey(v);
   }
   
   /**
    *  Adds a labeled edge to graph
    *  
    *  @param vFrom, a String representing edge source vertex
    *  @param vTo,   a String representing edge destination vertex
    *  @param label, a String representing edge label 
    *  @throws IllegalArgumentException when vTo or VFrom or label = ""
    *  @throws IllegalArgumentException when vFrom, vTo missing from graph
    *  @modifies (this)
    *  @effects add edge into list of edges associated with vFrom in graph
    *  @return return true if edge added successfully
    *
    *  Note: duplicate edges, labels are allowed
    *  
    */
   public void addEdge(String vFrom, String vTo, String label){
      checkRep();
      //vertex missing
      if (!vertices.containsKey(vFrom) ||
          !vertices.containsKey(vTo)){
         throwIAE("addVertex passed vertex not in graph");  
      }      
      //check for illegal String args
      check3args(vFrom, vTo, label);
      
      ArrayList<EdgeOut> sourceEdges = vertices.get(vFrom);
      EdgeOut add = new EdgeOut(vTo,label);
      sourceEdges.add(add);
      checkRep();
   }

   /**
    *  removes a labeled edge to graph
    *  
    *  @param vFrom, a String representing edge source vertex
    *  @param vTo,   a String representing edge destination vertex
    *  @param label, a String representing edge label
    *  
    *  @throws IllegalArgumentException when vFrom, vTo, == null
    *  @throws IllegalArgumentException when vFrom, vTo missing from graph
    *  @modifies (this)
    *  @effects remove edge from list of edges associated with vFrom in graph
    *  @return return true if edge added successfully
    *
    *  Note: duplicate edges, labels are allowed, removeEdge will only remove
    *        one EdgeOut equal to argument 
    *  
    */
   public boolean removeEdge(String vFrom, String vTo, String label){
      checkRep();
      
	   //vertex missing
      if (!vertices.containsKey(vFrom) ||
	       !vertices.containsKey(vTo)){
	      throwIAE("addVertex passed vertex not in graph");  
	   }
      
      //check for illegal String args
      check3args(vFrom, vTo, label);
	   
	   ArrayList<EdgeOut> sourceEdges = vertices.get(vFrom);
	   
	   for (EdgeOut e : sourceEdges){
	      if (e.get_label().equals(label) &&
	          e.get_vertexTo().equals(vTo)){
	         sourceEdges.remove(e);
	         //vertices.replace(vFrom, sourceEdges);
            checkRep();
	         return true;
	      }
	   }
	   return false;
	      
   }
   
   /**
    *  Returns true if this vertex v, has an EdgeOut equivalent to e
    *  
    *  @throws IllegalArgumentException when vFrom, vTo, == null
    *  @throws IllegalArgumentException when vFrom, vTo missing from graph
    *  @modifies (this)
    *  @return return true if edge exists in this graph
    * 
    */
   public boolean containsEdge(String vFrom, String vTo, String label){
      //vertex missing
      if (!vertices.containsKey(vFrom) ||
          !vertices.containsKey(vTo)){
         throwIAE("contains requires non-null, nonempty arg");  
      }
      //check for illegal String args
      check3args(vFrom, vTo, label);
      
      ArrayList<EdgeOut> sourceEdges = vertices.get(vFrom);
      
      for (EdgeOut e : sourceEdges){
         if (e.get_label().equals(label) &&
             e.get_vertexTo().equals(vTo)){
            return true;
         }
      }
      return false;
      
   }
   
   /**
    *  Returns children if vertex v
    *  
    *  @param v, a String representing vertex
    *  
    *  @throws IllegalArgumentException when v = null
    *  @throws IllegalArgumentException when v missing from graph
    *  @return return array of EdgeOut associated with v
    *          when v has no edges, returns empty list
    */
   public List<EdgeOut> getChildren(String v) {
      //vertex missing
      if (!vertices.containsKey(v)){
         throwIAE("getChildren arg vertex not in graph");  
      }
      //check for illegal args
      if (v == null){
         throwIAE("getChildren requires non-null, nonempty arg");  
      }
      
      return vertices.get(v);
   }
   
   /**
    *  Returns vertices as a Queue
    *  
    *  @return queue containing all vertices in graph
    *  
    *  Note: a graph with no vertices will return empty Queue
    */
   public Queue<String> getVerticesAsQueue(){
      Queue<String> verts = new LinkedList<String>();
      for(String s : vertices.keySet()){
         verts.add(s);
      }
      return verts;
   }
   
   /**
    *  Returns true iff graph is empty
    *  
    *  @return true when graph is empty
    */
   public boolean isEmpty(){
      return vertices.isEmpty();
   }
   
   /**
    *  Provides a string representation of graph:
    *  in form "vertex:(edge),(edge)\n"
    *     where "(edge) = (vertexTo,label);
    *  
    *  print eg: v1:(v2,x),(v3,y),(v4,z)
    *            v2:
    *            v3:(v1,m)
    *            v4:(v4,p)
    *  Note: empty graph will return empty string
    *  
    *  @return String representation of this graph
    */
   @Override
   public String toString(){
      StringBuilder s = new StringBuilder();
      for (String v : vertices.keySet()) {
         s.append(v + ":");
         ArrayList<EdgeOut> e = vertices.get(v);
         for (EdgeOut edge : e){
            s.append("(" + edge.get_vertexTo() + 
                     "," + edge.get_label() + ")" + ",");
         }
         if (s.charAt(s.length()-1) == ','){
            s.deleteCharAt(s.length()-1);
         }
         s.append("\n");
     }
     return s.toString();
   }
   
   /**
    *  clear the graph
    *  
    *  @effects empties (this) graph
    *  @modifies (this)
    */
   public void clear(){
      vertices = new HashMap<String,ArrayList<EdgeOut>>();
      checkRep();
   }
   
   /* 
    *  Checks 3 string args for null and empty string,
    *  throwing IAE if any arg is null
    *  
    */
   private void check3args(String s1, String s2, String s3){
    //illegal String args
      if (s1 == null || s2 == null || s3 == null){
         throwIAE("passed requires non-null, nonempty arg");
      }         
   }
   
   /*
    *  helper method to throw IllegalArgumentException with
    *  passed message
    *  
    *  @param s, string message of why exception occured
    */
   private void throwIAE(String s){
      throw new IllegalArgumentException(s);
   }
   
   /**
    *  Checks Representation Invariant holds as call.
    *  
    *  HashMap doesn't allow duplicate keys, 
    *  so RI holds without check for: graph contains unique v
    */
   private void checkRep(){
      
      assert(vertices != null) : "vertices is null";
      if (CHECK_FULL){
      for (String v : vertices.keySet()){
         assert(v != null) : "null vertex in graph";
         //long test
         
            ArrayList<EdgeOut> e = vertices.get(v);
            for (EdgeOut edge : e){
               assert (edge != null) : "null edge";
               assert (edge.get_label() != null);
               assert (!(edge.get_label().equals("")));
               assert (edge.get_vertexTo() != null);
               assert (!(edge.get_vertexTo().equals("")));
            }
         }
      }
      
   }
}
