package hw5;

/**
 * EdgeOut represents a graph edge destination vertex and label
 * 
 * EdgeOut is immutable
 * 
 * @author RyanMatthewSmith
 * @version 0.0
 */



public class EdgeOut implements Comparable<EdgeOut> {
   private final String vertexTo;
   private final String label;
   
   /**
    *  Abstraction Function:
    *     EdgeOut represents a vertex, v, and a label l such that:  
    *     v = representing a graph vertex, intended to be an edge destination
    *     l = representing an edge label
    *     
    *  Representation Invariant:
    *    v != null
    *    l != null
    */
	
   
   /**
    * Creates an EdgeOut
    * 
    * @param to, String representing destination vertex
    * @param label, String representing edge label
    * @requires 'to', 'label' != null
    * @effects constructs new EdgeOut, with 'to' as destination vertex
    *          and 'label' as edge label 
    */
   public EdgeOut(String to, String label) {   
	   this.vertexTo = to;
	   this.label = label;
	   checkRep();
   }
   
   /**
    * Returns a string of destination vertex
    * 
    * @return String representation of destination vertex
    */
   public String get_vertexTo() {
	   return vertexTo;
   }
   
   /**
    * Returns a string of edge label
    * 
    * @return String representation of label
    */
   public String get_label() {
	   return label;
   }
   
   /**
    *  String representation of EdgeOut
    *  in form "(vertexTo,label)"
    *  
    *  @returns string representation of EdgeOut
    */
   @Override
   public String toString(){
      return "(" + this.vertexTo + "," +
                   this.label + ")";
   }
   
   /**
    *  Overrides equals, returns equality comparison of EdgeOut
    *  
    *  @param o, EdgeOut to compare for equality with this
    *  @return true if both EdgeOut are equal
    */
   @Override
   public boolean equals(Object o){
      if (!(o instanceof EdgeOut)){
         return false;
      }
      EdgeOut in = (EdgeOut)o;
      return (this.label.equals(in.get_label()) && 
              this.vertexTo.equals(in.get_vertexTo()));
   }
   
   /**
    *  Compares two EdgeOut objects by alphabetical order,
    *  First looking at vertexTo, then label for ordering
    *  
    *  @param e, EdgeOut to compare to (this)
    *  @throws IllegalArgumentException when e is null or not an EdgeOut
    *  @returns a negative integer, zero, or a positive integer as this 
    *           object is less than, equal to, or greater than the 
    *           specified object.
    */
   @Override
   public int compareTo(EdgeOut e){
      int vert_compare = this.vertexTo.compareTo(e.get_vertexTo());
      int label_compare = this.label.compareTo(e.get_label());
      
      if (vert_compare < 0){
         return -1;
      } else if (vert_compare > 0){
         return 1;
      } else if (label_compare < 0){
         return -1;
      } else if (label_compare > 0){
         return 1;
      }      
      return 0;
   }
   
   /**
    *  Returns a hash code value for EdgeOut
    *  
    *  @returns hash code value for this EdgeOut
    */
   @Override
   public int hashCode(){
      return vertexTo.hashCode() + label.hashCode();
   }
   
   /**
    *  Checks if representation invariant holds
    */
   private void checkRep(){
      assert(vertexTo != null) : "vertexTo is null";
      assert(label != null) : "label is null";
   }
}
