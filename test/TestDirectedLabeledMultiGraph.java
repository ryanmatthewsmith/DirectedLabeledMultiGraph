package hw5.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.junit.Test;
import hw5.DirectedLabeledMultiGraph;
import hw5.EdgeOut;
/**
 * Test cases for DirectedLabeledMultiGraph
 * 
 * @author RyanMatthewSmith
 *
 */
public class TestDirectedLabeledMultiGraph {

   @Test
   public void testGraphConstructor() {
      new DirectedLabeledMultiGraph();
   }
   
   @Test
   public void testAddVertex() {
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      assertTrue("basic addVertex failed",myGraph.addVertex("v1"));
      assertTrue("basic addVertex failed",myGraph.addVertex("arbitrary string"));
   }
   
   
   @Test(expected=IllegalArgumentException.class)
   public void testAddNullVertex(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.addVertex(null);
   }
   
   @Test
   public void testAddVertexDuplicate() {
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      assertTrue("basic addVertex failed",myGraph.addVertex("v1"));
      assertFalse("duplicate vertex allowed",myGraph.addVertex("v1"));
      assertTrue("addVertex failed after dupe reject",myGraph.addVertex("v2"));
   }
   
   @Test
   public void testRemoveVertexDuplicate() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.addVertex("v3");
      assertTrue("removeVertex failed",myGraph.removeVertex("v1"));
      assertFalse("removeVertex failed",myGraph.containsVertex("v1"));
      assertTrue("removeVertex, removed unintended v",myGraph.containsVertex("v2"));
      assertTrue("remove vertex, removed unintended v",myGraph.containsVertex("v3"));
   }
   
   @Test(expected=IllegalArgumentException.class)
   public void testRemoveNullVertex(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.removeVertex(null);
   }
   
   @Test
   public void testContainsVertex() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      assertFalse("containsVertex false fail",myGraph.containsVertex("v3"));
      assertTrue("containsVertex true fail",myGraph.containsVertex("v1"));
   }
   
   @Test(expected=IllegalArgumentException.class)
   public void testContainsVertexNull(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.containsVertex(null);
   }
   
   @Test
   public void testAddEdgeSSL() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.addEdge("v1", "v2", "v1 to v2 edge");
      assertFalse("addEdge backwards",myGraph.containsEdge("v2", "v1", "v1 to v2 edge"));
      assertTrue("addEdge didn't add edge",myGraph.containsEdge("v1", "v2", "v1 to v2 edge"));
      
      myGraph.addEdge("v1", "v2", "dupe edge allowed");
      assertTrue("addEdge didn't allow multi edge",myGraph.containsEdge("v1", "v2", "dupe edge allowed"));
   }

   @Test(expected=IllegalArgumentException.class)
   public void test3ArgHelperNull(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.addEdge("a", "a",null);
   }
   @Test(expected=IllegalArgumentException.class)
   public void test3ArgHelperNull2(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.addEdge("a", null,"a");
   }
   @Test(expected=IllegalArgumentException.class)
   public void test3ArgHelperNull3(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.addEdge(null, "b","a");
   }  
   
   @Test
   public void testContainsEdgeSSL() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      assertFalse("containEdge",myGraph.containsEdge("v1", "v2", "x"));
      myGraph.addEdge("v1", "v2", "x");
      assertTrue("containEdge true fail",myGraph.containsEdge("v1", "v2", "x"));
   }
   
   @Test
   public void testRemoveEdgeSSL() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.addEdge("v1", "v2", "x");
      assertTrue("remove edge false",myGraph.removeEdge("v1","v2","x"));
      assertFalse("remove edge didn't remove edge",myGraph.containsEdge("v1", "v2", "x"));
      
      myGraph.addEdge("v1", "v2", "x");
      myGraph.addEdge("v1", "v2", "x");
      myGraph.removeEdge("v1","v2","x");
      assertTrue("remove edge removed extra edge",myGraph.containsEdge("v1", "v2", "x"));
   }
   
   @Test(expected=IllegalArgumentException.class)
   public void testRemoveEdgeNonVertex(){
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.removeEdge("v1","notIn","a");
   }  
   
   @Test
   public void testGetChildren() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.addEdge("v1", "v2", "x");
      myGraph.addEdge("v1", "v2", "x2");
      myGraph.addEdge("v1", "v2", "x2");
      List<EdgeOut> children = myGraph.getChildren("v1");
      
      int i = 0;
      for (EdgeOut e : children){
         assertTrue("children fail",e.get_vertexTo().equals("v2"));
         boolean c = (e.get_label().equals("x") ||
                      e.get_label().equals("x2") );
         i++;
         assertTrue(c);
      }
      assertEquals("incorrect amount of children",3,i);
   }
   
   @Test
   public void testVerticesAsQueue() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      Queue<String> vertices = myGraph.getVerticesAsQueue();
      
      String vertex1 = vertices.poll();
      String vertex2 = vertices.poll();
      String vertex3 = vertices.poll();
      
      assertTrue(vertex3 == null);
      assertTrue("Queue missing vertex",vertex1.equals("v1") || vertex1.equals("v2"));
      assertTrue("Queue missing vertex",vertex2.equals("v1") || vertex2.equals("v2"));
   }
   
   @Test
   public void testIsEmpty() {
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      assertTrue("isEmpty",myGraph.isEmpty());
      myGraph.addVertex("v1");
      assertFalse("isEmpty",myGraph.isEmpty());
   }
   
   @Test
   public void testClear() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.clear();
      assertTrue("clear failed",myGraph.isEmpty());
   }
   
   @Test
   public void testToString() {
      DirectedLabeledMultiGraph myGraph = twoVertexGraph();
      myGraph.addEdge("v1", "v2", "x");
      myGraph.addEdge("v1", "v2", "x2");
      myGraph.addEdge("v2", "v1", "x");
      String rep = "v1:(v2,x),(v2,x2)\nv2:(v1,x)\n";
      assertTrue(myGraph.toString().equals(rep));
      myGraph.clear();
      assertTrue(myGraph.toString().equals(""));
   }
   
   /*
    *  Create simple Graph with v1, v2 vertices 
    */
   private DirectedLabeledMultiGraph twoVertexGraph(){
      DirectedLabeledMultiGraph myGraph = new DirectedLabeledMultiGraph();
      myGraph.addVertex("v1");
      myGraph.addVertex("v2");
      return myGraph;
   }
   

}
