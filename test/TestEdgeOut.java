package hw5.test;
import static org.junit.Assert.*;

import org.junit.Test;
import hw5.EdgeOut;
/**
 * Tests for EdgesOut
 * 
 * @author RyanMatthewSmith
 *
 */
public class TestEdgeOut {

   @Test
   public void testEdgesOutConstructor() {
      new EdgeOut("a","b");
      new EdgeOut("!","!");     
   }

   
   @Test
   public void test_get_vertexTo(){
      EdgeOut edge1 = new EdgeOut("alpha", "beta");
      EdgeOut edge2 = new EdgeOut("!!", "zip");
      
      assertEquals("alpha failed get_vertexTo",edge1.get_vertexTo(),"alpha");
      assertEquals("!! failed get_vertexTo", edge2.get_vertexTo(), "!!");
   }
   
   @Test
   public void test_get_label(){
      EdgeOut edge1 = new EdgeOut("alpha", "beta");
      EdgeOut edge2 = new EdgeOut("!!", "zip");
      
      assertEquals("beta failed get_label",edge1.get_label(),"beta");
      assertEquals("zip failed get_label", edge2.get_label(), "zip");
   }
   
   @Test
   public void test_toString(){
      EdgeOut edge1 = new EdgeOut("happy", "go lucky");      
      assertEquals("beta failed get_label",edge1.toString(),"(happy,go lucky)");
   }
   
   @Test
   public void test_compareTo(){
      EdgeOut edge1 = new EdgeOut("b", "b"); 
      EdgeOut edge2 = new EdgeOut("b", "b"); 
      EdgeOut edge3 = new EdgeOut("a", "b"); 
      EdgeOut edge4 = new EdgeOut("c", "b"); 
      EdgeOut edge5 = new EdgeOut("b", "a");
      EdgeOut edge6 = new EdgeOut("b", "c"); 
      EdgeOut edge7 = new EdgeOut("a", "a"); 
      EdgeOut edge8 = new EdgeOut("c", "c"); 
      
      assertTrue("compareTo fail =", edge1.compareTo(edge2) == 0);
      assertTrue("compareTo fail v <", edge1.compareTo(edge3) > 0);
      assertTrue("compareTo fail v >", edge1.compareTo(edge4) < 0);
      assertTrue("compareTo fail l <", edge1.compareTo(edge5) > 0);
      assertTrue("compareTo fail l >", edge1.compareTo(edge6) < 0);
      assertTrue("compareTo fail both <", edge1.compareTo(edge7) > 0);
      assertTrue("compareTo fail both >", edge1.compareTo(edge8) < 0);
   }
   
   @Test
   public void test_equals(){
      EdgeOut edge1 = new EdgeOut("a", "a"); 
      EdgeOut edge2 = new EdgeOut("a", "b"); 
      EdgeOut edge3 = new EdgeOut("b", "a"); 
      EdgeOut edge4 = new EdgeOut("a", "a"); 
      EdgeOut edge5 = new EdgeOut("c", "c"); 
      
      assertFalse("equals fail only v =",edge1.equals(edge2));
      assertFalse("equals fail only l =",edge1.equals(edge3));
      assertFalse("equals fail no =",edge1.equals(edge5));
      assertTrue("equals fail actually =", edge1.equals(edge4));
      assertFalse("equals fail wrong class", edge1.equals(new Object()));
   }
   

}
